defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state -> 
    # Hound.end_session
    nil
  end


  given_ ~r/^the following list of books borrowed by active users$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I want to extend time period for "(?<argument_one>[^"]+)" $/,
  fn state, %{argument_one: _argument_one} ->
    {:ok, state}
  end

  and_ ~r/^I open LBBS' web page$/, fn state ->
    navigate_to "/books"
    {:ok, state}
  end

  and_ ~r/^I select the book I want to extend the time period$/, fn state ->
    click({:id, "extend"})
    {:ok, state}
  end

  when_ ~r/^I submit the extension request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should see the extension accepted message and updated due date$/, fn state ->
    :timer.sleep(2000)
    assert visible_in_page? ~r/Extension accepted successfully./
    {:ok, state}
  end

  then_ ~r/^I should receive a rejection message with no updates in due date$/, fn state ->
    :timer.sleep(2000)
    assert visible_in_page? ~r/Extension Rejected!/
    {:ok, state}
  end

end
