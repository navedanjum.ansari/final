As a user
  Such that I extend the borrow period for a book
  I want to extend the time period for a book

  Scenario: The book loan period extension via LBBS' is successfully accepted
    Given the following list of books borrowed by active users
        | Title                            | Code     |  Start_date   |  Due_date    | Time_extended |
        | Programming Phoenix 1.3          |  A       | 09-01-2018    | 16-01-2018   |  0            |
        | Secrets of JavaScript ninja      |  A       | 19-12-2017    | 16-01-2018   |  4            |
  
    
    And I want to extend time period for "Programming Phoenix 1.3" 
    And I open LBBS' web page
    And I select the book I want to extend the time period
    When I submit the extension request
    Then I should see the extension accepted message and updated due date


Scenario: The book loan period extension via LBBS' is rejected
    Given the following list of books borrowed by active users
        | Title                            | Code     |  Start_date   |  Due_date    | Time_extended |
        | Programming Phoenix 1.3          |  A       | 09-01-2018    | 16-01-2018   |  0            |
        | Secrets of JavaScript ninja      |  A       | 19-12-2017    | 16-01-2018   |  4            |


    And I want to extend time period for "Secrets of JavaScript ninja" 
    And I open LBBS' web page
    And I select the book I want to extend the time period
    When I submit the extension request
    Then I should receive a rejection message with no updates in due date