import Vue from "vue";
import axios from "axios";
import VueRouter from "vue-router";

import books from "./books";

Vue.use(VueRouter);

Vue.component("books", books);

var router = new VueRouter({
    routes: [
        { path: '/', component: books }
    ]
})

new Vue({
    router
}).$mount("#final-app");
