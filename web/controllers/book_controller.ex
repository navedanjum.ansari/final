defmodule Final.BookController do
  use Final.Web, :controller
  alias Ecto.{Changeset}
  alias Final.Book

  #Display on first page
  def index(conn, _params) do
    books = Repo.all(Book)
    render(conn, "index.html", books: books)
  end

 #Show function to show updated extension
  def show(conn, %{"id" => id}) do
    book = Repo.get!(Book, id)
    render(conn, "show.html", book: book)
  end

  #Edit is the time extension function
  def edit(conn, %{"id" => id}) do
    book = Repo.get!(Book, id)
    changeset = Book.changeset(book)
    render(conn, "edit.html", book: book, changeset: changeset)
  end


  def update(conn, %{"id" => id}) do
    book = Repo.get!(Book, id)
        if (book.times_extended == 4) do
            conn
            |> put_flash(:info, "Extension Rejected!")
            |> redirect(to: book_path(conn, :show, book))
        else
            Book.changeset(book)
            |> Changeset.put_change(:times_extended, book.times_extended+1)
            |> Changeset.put_change(:due_date, Date.add(book.due_date, 7))
            |> Repo.update!

            conn
            |> put_flash(:info, "Extension accepted successfully.")
            |> redirect(to: book_path(conn, :show, book))
        end
  end

end
