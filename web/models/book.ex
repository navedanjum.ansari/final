defmodule Final.Book do
  use Final.Web, :model

  schema "books" do
    field :title, :string
    field :code, :string
    field :start_time, :date
    field :due_date, :date
    field :times_extended, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :code, :start_time, :due_date, :times_extended])
    |> validate_required([:title, :code, :start_time, :due_date, :times_extended])
    |> unique_constraint(:title)
  end
end
