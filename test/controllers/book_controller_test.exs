defmodule Final.BookControllerTest do
  use Final.ConnCase
  alias Final.{Book, Repo}

  @valid_attrs %{code: "A", due_date: ~D[2018-01-16], start_time: ~D[2018-01-09], times_extended: 0, title: "Programming Phoenix 1.3"}
  @invalid_attrs %{}

  test "PUT book date extention - Success Scenario", %{conn: conn} do
   [%{title: "Programming Phoenix 1.3", code: "A", start_time: ~D[2018-01-09], due_date: ~D[2018-01-16], times_extended: 0}]
   |> Enum.map(fn book_data -> Book.changeset(%Book{}, book_data) end)
   |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    query = from a in Book, where: a.title == "Programming Phoenix 1.3", select: a
    row_book = Repo.one(query)
    conn = put conn, "/books/" <> to_string(row_book.id), %{}
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ ~r/Extension accepted successfully./
    query = from a in Book, where: a.title == "Programming Phoenix 1.3", select: a
    row_updated_book = Repo.one(query)
    #Code A - 7 days added
    assert Date.compare(row_updated_book.due_date, Date.add(~D[2018-01-16], 7)) == :eq

  end

 
  test "PUT book date extention - Rejection Scenario", %{conn: conn} do
    [%{title: "Secrets of JavaScript ninja", code: "A", start_time: ~D[2017-12-19], due_date: ~D[2018-01-16], times_extended: 4}]
    |> Enum.map(fn book_data -> Book.changeset(%Book{}, book_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    query = from a in Book, where: a.title == "Secrets of JavaScript ninja", select: a
    row_book = Repo.one(query)
    conn = put conn, "/books/" <> to_string(row_book.id), %{}
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ ~r/Extension Rejected!/
    query = from a in Book, where: a.title == "Secrets of JavaScript ninja", select: a
    row_updated_book = Repo.one(query)
    # Extension failure
    assert Date.compare(row_updated_book.due_date, ~D[2018-01-16]) == :eq

  end
 
end
