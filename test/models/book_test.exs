defmodule Final.BookTest do
  use Final.ModelCase

  alias Final.Book

  @valid_attrs %{code: "some code", due_date: ~D[2010-04-17], start_time: ~D[2010-04-17], times_extended: 42, title: "some title"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Book.changeset(%Book{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Book.changeset(%Book{}, @invalid_attrs)
    refute changeset.valid?
  end
end
