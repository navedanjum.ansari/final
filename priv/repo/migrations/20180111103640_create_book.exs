defmodule Final.Repo.Migrations.CreateBook do
  use Ecto.Migration

  def change do
    create table(:books) do
      add :title, :string
      add :code, :string
      add :start_time, :date
      add :due_date, :date
      add :times_extended, :integer

      timestamps()
    end

    create unique_index(:books, [:title])
  end
end
