# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Final.Repo.insert!(%Final.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will halt execution if something goes wrong.

alias Final.{Repo,Book}

books = [
  %{title: "Programming Phoenix 1.3", code: "A", start_time: "2018-01-09", due_date: "2018-01-16", times_extended: 0},
  %{title: "Secrets of JavaScript ninja", code: "A", start_time: "2017-12-19", due_date: "2018-01-16", times_extended: 4},
  %{title: "The Little Princess", code: "B", start_time: "2017-12-19", due_date: "2018-01-19", times_extended: 1},
  %{title: "The Return of the King", code: "B", start_time: "2017-12-19", due_date: "2018-01-16", times_extended: 2},
  %{title: "The Two Towers", code: "C", start_time: "2018-01-02", due_date: "2018-01-22", times_extended: 0},
  %{title: "The Fellowship of the Ring", code: "C", start_time: "2018-01-01", due_date: "2018-01-22", times_extended: 0}
  ]
 Enum.each(books, fn(params) ->
 Final.Book.changeset(%Final.Book{}, params)
 |> Final.Repo.insert!()
 end)

